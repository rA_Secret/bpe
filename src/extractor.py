#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2011-2013 Yommys
# Copyright (C) 2016 4144

from src.asm import Asm
from src.exe import Exe
from src.math import Math


class Extractor:
    template = \
        "\xE8\xAB\xAB\xAB\xAB" \
      + "\x8B\xC8"          \
      + "\xE8\xAB\xAB\xAB\xAB"

    def packets_php(self, fileName):
        called = None
        exe = Exe()
        exe.load(fileName)
        if exe.client_date >= 20120710:
            print "new clients"
            called = False
            code = "\x55" \
                   "\x8B\xEC" \
                   "\x83\xEC\xAB" \
                   "\x56" \
                   "\x8B\xF1" \
                   "\xE8\xAB\xAB\xAB\xAB" \
                   "\xB8"
        else:
            print "old clients"
            code = "\x55" \
                   "\x8B\xEC" \
                   "\x83\xE4\xF8" \
                   "\x83\xEC\xAB" \
                   "\x56" \
                   "\x8B\xF1" \
                   "\xB8"

#        section = exe.detectSection(code, "\xAB")
        offset = exe.code(code, "\xAB")
        if offset == False:
            print "Error: failed in part 1"
            exit(1)
        print fileName
        print "part 1 ok"


    def getAddr(self, offset, addrOffset, instructionOffset):
        ptr = self.exe.read(offset + addrOffset, 4, "V")
        return Math.sumOffset(offset + instructionOffset, ptr)


    def run4Step(self, code, num, addrOffset, instructionOffset):
        offset = self.exe.code1(code, "\xAB", self.initPacketMapFunction, self.initPacketMapFunction + 0x140)
        if offset != False:
            self.exe.printAddr("peek4.{0}".format(num), offset)
            self.initPacketLenWithClientFunction = self.getAddr(offset, addrOffset, instructionOffset)
        return offset


    def run4StepAlone(self, code, num, addrOffset, instructionOffset):
        offset = self.exe.code1(code, "\xAB")
        if offset != False:
            self.exe.printAddr("peek4.{0}".format(num), offset)
            self.initPacketLenWithClientFunction = self.getAddr(offset, addrOffset, instructionOffset)
        return offset


    def getBasicInfo(self):
        # Step 1 - Find the GetPacketSize function call
        code = \
            self.template \
          + "\x50"    \
          + self.template \
          + "\x6A\x01" \
          + self.template \
          + "\x6A\x06"
        offset = self.exe.code1(code, "\xAB")
        if offset == False:
            code = \
                self.template \
              + "\x50"    \
              + self.template \
              + "\x6A\x01" \
              + self.template \
              + "\xe9"
            offset = self.exe.code1(code, "\xAB")
        if offset != False:
            self.exe.printAddr("peek1", offset)
            self.instanceR = self.getAddr(offset, 1, 5)
            self.exe.printAddr("CRagConnection::instanceR", self.instanceR)

            # Step 2a - Go Inside the GetPacketSize function
            offset = offset + len(self.template) - 4
            # addr + offset + 4 in 32 bit math
            offset = self.getAddr(offset, 0, 4)
            self.getPacketSizeFunction = offset
            self.exe.printAddr("getPacketSize", self.getPacketSizeFunction)
            code = \
                "\xB9\xAB\xAB\xAB\x00" \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\x8B\xAB\x04"
            offset0 = offset
            offset = self.exe.code1(code, "\xAB", offset, offset + 0x80)
        else:
            # 1.2
            # push 1
            # call CRagConnection::instanceR
            # mov ecx, eax
            # call CConnection::SetBlock (probably)
            # push 6
            code = \
                "\x6a\x01" \
                "\xe8\xAB\xAB\xAB\xAB" \
                "\x8B\xC8" \
                "\xe8\xAB\xAB\xAB\xAB" \
                "\x6A\x06"
            offset = self.exe.code1(code, "\xAB")
            if offset != False:
                self.exe.printAddr("peek1.2", offset)
                self.instanceR = self.getAddr(offset, 3, 7)
                self.exe.printAddr("CRagConnection::instanceR", self.instanceR)
                # skip step 2 and 3 becase no signatures for it. Go to 4.
                offset = False
            else:
                # in very old client no way to find any functions except initPacketLenWithClient
                # but functions can be wrong :(
                if self.exe.client_date <= 20031124:
                    # mov [esi + N], ebx
                    # mov [esi + N], ebx
                    # mov dword ptr [esi], N
                    # call initPacketLenWithClient
                    # mov ecx, [ebp + N]
                    offset = self.run4StepAlone(
                        "\x89\x9E\xAB\xAB\xAB\xAB" \
                        "\x89\x9E\xAB\xAB\xAB\xAB" \
                        "\xC7\x06\xAB\xAB\xAB\xAB" \
                        "\xE8\xAB\xAB\xAB\xAB" \
                        "\x8B\x4D\xAB",
                        8,
                        19, 23)
                elif self.exe.client_date <= 20041207:
                    # mov [esi + N], M
                    # mov [esi + N], M
                    # mov dword ptr [esi], N
                    # call initPacketLenWithClient
                    # mov ecx, [ebp + N]
                    offset = self.run4StepAlone(
                        "\x89\x9e\xAB\xAB\xAB\xAB" \
                        "\x89\x9e\xAB\xAB\xAB\xAB" \
                        "\xC7\x06\xAB\xAB\xAB\xAB" \
                        "\xE8\xAB\xAB\xAB\xAB" \
                        "\x8B\x4D\xAB",
                        7,
                        19, 23)
                    # mov ecx, esi
                    # mov byte ptr [ebp + N], M
                    # mov dword ptr [esi], N
                    # call initPacketLenWithClient
                    # mov ecx, [ebp + N]
                    if offset == False:
                        offset = self.run4StepAlone(
                            "\x8B\xCE" \
                            "\xC6\x45\xAB\xAB" \
                            "\xC7\x06\xAB\xAB\xAB\xAB" \
                            "\xE8\xAB\xAB\xAB\xAB" \
                            "\x8B\x4D\xAB",
                            9,
                            13, 17)
                if offset == False:
                    self.exe.log("failed in 1")
                    return False
                self.exe.printAddr("initPacketLenWithClient", self.initPacketLenWithClientFunction)
                return True
        if offset != False:
            self.exe.printAddr("peek2", offset)
            #Step 2c - Extract the g_PacketLenMap assignment
            self.gPacketLenMap = self.exe.read(offset + 1, 4)
            self.pktLenFunction = self.getAddr(offset, 6, 10)
            self.exe.printAddr("pktLen", self.pktLenFunction)

            # 3
            code = \
                self.gPacketLenMap + \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\x68\xAB\xAB\xAB\x00" \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\x59" \
                "\xC3"
            offset = self.exe.code1(code, "\xAB")
            if offset != False:
                self.exe.printAddr("peek3", offset)
                offset = offset - 1
                self.initPacketMapFunction = self.getAddr(offset, 6, 10)
            else:
                # 3.2
                # mov RR, g_packetLenMap
                # call CRagConnection::InitPacketMap
                # push addr1
                # call addr2
                # add esp, N
                # mov esp, ebp
                # pop ebp
                # retn
                code = \
                    self.gPacketLenMap + \
                    "\xE8\xAB\xAB\xAB\xAB" \
                    "\x68\xAB\xAB\xAB\x00" \
                    "\xE8\xAB\xAB\xAB\xAB" \
                    "\x83\xC4\xAB" \
                    "\x8B\xE5" \
                    "\x5d" \
                    "\xC3"
                offset = self.exe.code1(code, "\xAB")
                if offset == False:
                    self.exe.log("failed in 3")
                    return False
                self.exe.printAddr("peek3.2", offset)
                offset = offset - 1
                self.initPacketMapFunction = self.getAddr(offset, 6, 10)

        else:
            self.gPacketLenMap = None
            # search in CRagConnection::instanceR for call to InitPacketMap
            # ...
            # 2a.2
            # call CRagConnection::InitPacketMap
            # push addr1
            # call addr2
            code = \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\x68\xAB\xAB\xAB\x00" \
                "\xE8\xAB\xAB\xAB\xAB"
            offset = self.exe.code1(code, "\xAB", self.instanceR, self.instanceR + 0x50)
            if offset == False:
                self.exe.log("failed in 2a")
                return False
            self.exe.printAddr("peek2a.2", offset)
            self.initPacketMapFunction = self.getAddr(offset, 1, 5)

        # 4
        self.exe.printAddr("initPacketMap", self.initPacketMapFunction)
        # peek step 4
        res = self.run4Step(
            "\x8B\xCE" \
            "\xE8\xAB\xAB\xAB\xAB" \
            "\xC7",
            1,
            3, 7)
        if res == False:
            # 4c.2
            # mov ecx, esi
            # mov [ebp + N], ebx
            # call initPacketLenWithClient
            # mov ecx, [ebp + N]
            res = self.run4Step(
                "\x8B\xCE" \
                "\x89\x5D\xAB" \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\x8B\x4D\xAB",
                2,
                6, 10)
        if res == False:
            # 4c.3
            # mov ecx, esi
            # mov [ebp + N], M
            # call initPacketLenWithClient
            # mov ecx, [ebp + N]
            res = self.run4Step(
                "\x8B\xCE" \
                "\xC7\x45\xAB\xAB\xAB\xAB\xAB" \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\x8B\x4D\xAB",
                3,
                10, 14)
        if res == False:
            # 4c.4
            # mov [ebp + N], M
            # mov dword ptr [esi], N
            # call initPacketLenWithClient
            # mov ecx, [ebp + N]
            res = self.run4Step(
                "\xC7\x45\xAB\xAB\xAB\xAB\xAB" \
                "\xC7\x06\xAB\xAB\xAB\xAB" \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\x8B\x4D\xAB",
                4,
                14, 18)
        if res == False:
            # 4c.5
            # mov [esp + N], M
            # mov dword ptr [esi], N
            # call initPacketLenWithClient
            # mov [esp + N], M
            res = self.run4Step(
                "\xC7\x44\x24\xAB\xAB\xAB\xAB\xAB" \
                "\xC7\x06\xAB\xAB\xAB\xAB" \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\xC7\x44\x24\xAB\xAB\xAB\xAB\xAB",
                5,
                15, 19)
        if res == False:
            # 4c.6
            # mov [esp + N], M
            # mov dword ptr [esi], N
            # call initPacketLenWithClient
            # mov eax, esi
            # mov ecx, [esp + N]
            res = self.run4Step(
                "\xC7\x44\x24\xAB\xAB\xAB\xAB\xAB" \
                "\xC7\x06\xAB\xAB\xAB\xAB" \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\x8B\xC6" \
                "\x8B\x4C\x24\xAB",
                6,
                15, 19)
        if res == False:
            self.exe.log("failed in 4")
            return False
        self.exe.printAddr("initPacketLenWithClient", self.initPacketLenWithClientFunction)
        return True


    def walkUntilRet(self):
        asm = Asm()
        asm.exe = self.exe
        asm.init(self.initPacketLenWithClientFunction)
        #asm.printLine()
        while asm.move() == True:
            #asm.printLine()
            pass
        print "Asm code walked"


    def runAsmSimple(self):
        asm = Asm()
        asm.debug = False
        asm.debugMemory = False
        asm.exe = self.exe
        asm.init(self.initPacketLenWithClientFunction)
        asm.initState(0)
        #asm.printRegisters()
        #asm.printLine()
        asm.run()
        #asm.printRegisters()
        while asm.move() == True:
            #asm.printLine()
            asm.run()
            #asm.printRegisters()
            pass
        self.stackAlign = asm.popCounter
        self.callFunctions = asm.callFunctions
        self.allCallFunctions = asm.callFunctions
        print "Asm simple code complete"
        #print asm.memory


    def runAsmCollect(self):
        asm = Asm()
        asm.debug = False
        asm.debugMemory = False
        asm.stackAlign = self.stackAlign
        asm.exe = self.exe
        asm.init(self.initPacketLenWithClientFunction)
        asm.initState(1)
        asm.callFunctions = self.callFunctions
        asm.allCallFunctions = self.allCallFunctions
        #asm.printRegisters()
        #asm.printMemory()
        #asm.printLine()
        asm.run()
        while asm.move() == True:
            asm.run()
            pass
        self.packets = asm.packets
        self.callFunctions = asm.callFunctions
        print "Asm collector code complete"


    def parseCallStack(self):
        correctFunctions = dict()
        for addr in self.callFunctions:
            callFunction = self.callFunctions[addr]
            if callFunction.callCounter < 10:
                continue
            correctCounts = dict()
            tmpPushes = 0
            # walk all push counts and remove used very rare
            for pushCount in callFunction.counts:
                num = callFunction.counts[pushCount]
                if num > 10:
                    correctCounts[pushCount] = num
                    tmpPushes = pushCount
            callFunction.counts = correctCounts
            # need at least two different function calls in initPacketMapWithClient
            if len(callFunction.counts) > 1:
                self.exe.log("Error. Different number of pushes before function call: {0}\n{1}".format(self.exe.getAddr(addr), callFunction.counts))
                exit(1)
            callFunction.adjustSp = tmpPushes
            #print "addr {0}, pushes {1}".format(addr, callFunction.adjustSp)
            correctFunctions[addr] = callFunction
        self.callFunctions = correctFunctions


    def checkPackets(self):
        error = False
        for key in self.callFunctions:
            func = self.callFunctions[key]
            if len(func.collectCallsFrom) < 1:
                continue
            for calladdr in func.callsFrom:
                if calladdr not in func.collectCallsFrom:
                    print "Error function was not called from addr: {0}".format(self.exe.getAddr(calladdr))
                    error = True
        if error == True:
            exit(1)


        if len(self.packets) < 380:
            self.exe.log("Error. too small packets amount: {0}".format(len(self.packets)))
            exit(1)


    def savePackets(self):
        self.exe.log("Packets number: {0}".format(len(self.packets)))
        processedKeys = dict()
        with open("{0}/bpe_data_{1}.ini".format(self.exe.outDir, self.exe.client_date), "wt") as w:
            for packet in self.packets:
                packetId = hex(packet[0]).upper()
                if packet[0] in processedKeys:
                    newLen = processedKeys[packet[0]][1]
                    if newLen == packet[1]:
                        continue
                    self.exe.log("Warning. Duplicate packet with different size: {0}: {1} vs {2}".format(hex(packet[0]), packet[1], newLen))
                while len(packetId) < 6:
                    packetId = packetId[:2] + "0" + packetId[2:]
                packetId = packetId[0] + "x" + packetId[2:]
                w.write("{0},{1},{2},{3}\n".format(packetId, packet[1], packet[2], packet[3]))
                processedKeys[packet[0]] = packet
        processedKeys = dict()
        cleanPackets = dict()
        for packet in self.packets:
            cleanPackets[packet[0]] = packet
        with open("{0}/bpe_PacketLengths_{1}.ini".format(self.exe.outDir, self.exe.client_date), "wt") as w:
            w.write("[Packet_Lengths]\r\n")
            for packet in self.packets:
                packet = cleanPackets[packet[0]]
                packetId = hex(packet[0]).upper()
                if packet[0] in processedKeys:
                    newLen = processedKeys[packet[0]][1]
                    if newLen == packet[1]:
                        continue
                    self.exe.log("Warning. Duplicate packet with different size: {0}: {1} vs {2}".format(hex(packet[0]), packet[1], newLen))
                while len(packetId) < 6:
                    packetId = packetId[:2] + "0" + packetId[2:]
                packetId = packetId[0] + "x" + packetId[2:]
                w.write("{0} = {1}\r\n".format(packetId, packet[1]))
                processedKeys[packet[0]] = packet
            w.write("[Shuffle_Packets]\r\n")


    def getpackets(self, fileName):
        exe = Exe()
        if exe.load(fileName) == False:
            print "Skipping file {0}".format(fileName)
            exit(1)
        self.exe = exe
        parsed = self.getBasicInfo()
        if parsed == False:
            self.exe.log("Error: basic parsing info failed")
            exit(1)
        self.walkUntilRet()
        self.runAsmSimple()
        self.parseCallStack()
        self.runAsmCollect()
        self.checkPackets()
        self.savePackets()
