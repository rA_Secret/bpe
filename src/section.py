#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016 4144

class Section:
    def printNum(self, text, num):
        print "{0}: {1}, {2}".format(text, num, hex(num))


    def exeAddrToAbs(self, offset):
        return offset - self.rOffset + self.image_base + self.vOffset


    def printAddr(self, text, offset):
        return "{0}: {1}".format(text, hex(self.exeAddrToAbs(offset)))


    def getAddr(self, offset):
        return "{0}:{1}".format(self.name, hex(self.exeAddrToAbs(offset)))


    def printAddrInfo(self, offset):
        self.printNum("exe offset", offset)
        self.printNum("section offset", offset - self.rOffset)
        self.printNum("debug offset", self.exeAddrToAbs(offset))
        #self.printAll()


    def printAll(self):
        print "Section offsets:"
        print hex(self.vSize)
        print hex(self.vOffset)
        print hex(self.vEnd)
        print hex(self.rSize)
        print hex(self.rOffset)
        print hex(self.rEnd)
        print hex(self.vrDiff)
        print hex(self.align)
