#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016 4144

class Math:
    @staticmethod
    def sumOffset(pointer, offset):
        return (pointer + offset) & 0xffffffff

    @staticmethod
    def extendByteToInt(value):
        return value & 0xffffffff
