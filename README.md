Batch packet extractor for old kro clients.

Supported client versions from 2003-10-28 to 2012-07-02 with some exceptions.

For batch extract:

1. put clients in clients directory.

2. run ./main.py

3. you will get extracted packets in output directory.

For single extract:

1. put client in clients directory.

2. run ./main.py CLIENTNAME.exe

3. you will get extracted packets in output directory.
